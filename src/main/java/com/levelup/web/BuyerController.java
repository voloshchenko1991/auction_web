package com.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by Danyl on 010 10.10.17.
 */
@Controller
public class BuyerController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showIndex(){
        return "static/index.html";
    }

    @Autowired
    ValidatorBuyer validatorBuyer;


    @InitBinder
    private void initBinderBuyer(WebDataBinder binder) {
        binder.setValidator(validatorBuyer);
    }



    @RequestMapping(value = "/buyer", method = RequestMethod.GET)
    public String acceptFormBuyer(@Valid FormBuyer formBuyer, BindingResult bindingResult){
        String result = "static/index.html";
        if(!bindingResult.hasErrors()) {
            result = "static/indexSuccess.html";
        }else if( bindingResult.hasFieldErrors("buyerPhone") ){
            result = "static/ErrorPhone.html";
        }else if( bindingResult.hasFieldErrors("buyerName") ){
            result = "static/ErrorName.html";
        }else{
            result = "static/EmptyError.html";
        }
        return result;
    }



}
