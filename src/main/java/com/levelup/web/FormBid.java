package com.levelup.web;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Data
public class FormBid {

    @NotNull
    private String sellerID;
    @NotNull
    private String buyerID;
    @NotNull
    private String productID;
    @NotNull
    private String bid;

}
