package com.levelup.web;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Data
public class FormBuyer {
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private String buyerRegNumber;
}
