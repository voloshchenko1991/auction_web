package com.levelup.web;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Data
public class FormProduct {
    private String productName;
    private String productCategory;
    private String productPrice;
    private String sellerID;
}
