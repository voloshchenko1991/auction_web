package com.levelup.web;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Data
public class FormSeller {
    @NotNull
    private String sellerName;
    @NotNull
    private String sellerPhone;
    @NotNull
    private String sellerRegNumber;
}
