package com.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Controller
public class ProductController {

    @Autowired
    ValidatorProduct validatorProduct;


    @InitBinder
    private void initBinderProduct(WebDataBinder binder) {
        binder.setValidator(validatorProduct);
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String acceptFormProduct(@Valid FormProduct formProduct, BindingResult bindingResult){
        String result = "static/index.html";
        if(!bindingResult.hasErrors()) {
            result = "static/indexSuccess.html";
        }else if( bindingResult.hasFieldErrors("productPrice") ){
            result = "static/EmptyError.html";
        }else if( bindingResult.hasFieldErrors("sellerID") ){
            result = "static/ErrorSellerID.html";
        }
        return result;
    }
}
