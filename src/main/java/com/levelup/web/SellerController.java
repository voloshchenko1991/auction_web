package com.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Controller
public class SellerController {

    @Autowired
    ValidatorSeller validatorSeller;


    @InitBinder
    private void initBinderSeller(WebDataBinder binder) {
        binder.setValidator(validatorSeller);
    }

    @RequestMapping(value = "/seller", method = RequestMethod.GET)
    public String acceptFormSeller(@Valid FormSeller formSeller, BindingResult bindingResult){
        String result = "static/index.html";
        if(!bindingResult.hasErrors()) {
            result = "static/indexSuccess.html";
        }else if( bindingResult.hasFieldErrors("sellerPhone") ){
            result = "static/ErrorPhone.html";
        }else if( bindingResult.hasFieldErrors("sellerName") ){
            result = "static/ErrorName.html";
        }else{
            result = "static/EmptyError.html";
        }
        return result;
    }
}
