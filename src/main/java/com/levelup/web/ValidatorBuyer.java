package com.levelup.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Component
public class ValidatorBuyer implements Validator {

    public boolean supports(Class<?> aClass) {
        return FormBuyer.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        FormBuyer formBuyer = (FormBuyer) o;
        Pattern phoneRegExp = Pattern.compile("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$");
        Pattern nameRegExp = Pattern.compile("^[a-zA-Z]+$");

        if (    formBuyer.getBuyerName().isEmpty() ||
                formBuyer.getBuyerAddress().isEmpty() ||
                formBuyer.getBuyerPhone().isEmpty() ||
                formBuyer.getBuyerRegNumber().isEmpty() ) {
            errors.rejectValue("buyerName", "601", "Missed field");
        }

        Matcher matcher = phoneRegExp.matcher( formBuyer.getBuyerPhone() );

        if(formBuyer.getBuyerPhone().length() < 10){
            errors.rejectValue("buyerPhone", "602", "Invalid phone number. Lenght must be 10 digits");
        }

        if( !matcher.matches() ){
            errors.rejectValue("buyerPhone", "603", "Invalid phone number format");
        }

        matcher = nameRegExp.matcher( formBuyer.getBuyerName() );
        if( !matcher.matches() ){
            errors.rejectValue("buyerName", "604", "Name must consist of letters only!");
        }
    }
}
