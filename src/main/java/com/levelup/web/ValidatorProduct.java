package com.levelup.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Component
public class ValidatorProduct implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return FormProduct.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        FormProduct formProduct = (FormProduct) o;
        Pattern idRegExp = Pattern.compile("^[1-9]\\d*$");

        if (    formProduct.getProductName().isEmpty() ||
                formProduct.getProductCategory().isEmpty() ||
                formProduct.getProductPrice().isEmpty() ||
                formProduct.getSellerID().isEmpty() ) {
            errors.rejectValue("productPrice", "601", "Missed field");
        }

        Matcher matcher = idRegExp.matcher( formProduct.getSellerID() );


        if( !matcher.matches() ){
            errors.rejectValue("sellerID", "604", "Name must consist of digits only!");
        }
    }
}
