package com.levelup.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Danyl on 011 11.10.17.
 */
@Component
public class ValidatorSeller implements Validator {

    public boolean supports(Class<?> aClass) {
        return FormSeller.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        FormSeller formSeller = (FormSeller) o;
        Pattern phoneRegExp = Pattern.compile("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$");
        Pattern nameRegExp = Pattern.compile("^[a-zA-Z]+$");

        if (    formSeller.getSellerName().isEmpty() ||
                formSeller.getSellerPhone().isEmpty() ||
                formSeller.getSellerRegNumber().isEmpty() ) {
            errors.rejectValue("sellerName", "601", "Missed field");
        }

        Matcher matcher = phoneRegExp.matcher( formSeller.getSellerPhone() );

        if(formSeller.getSellerPhone().length() < 10){
            errors.rejectValue("sellerPhone", "602", "Invalid phone number. Lenght must be 10 digits");
        }

        if( !matcher.matches() ){
            errors.rejectValue("sellerPhone", "603", "Invalid phone number format");
        }

        matcher = nameRegExp.matcher( formSeller.getSellerName() );
        if( !matcher.matches() ){
            errors.rejectValue("sellerName", "604", "Name must consist of letters only!");
        }
    }
}

